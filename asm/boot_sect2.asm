;
; A simple boot sector program that prints some characters
; to the screen using the BIOS interrupt 0x10 and its service
; 0x0e (Write character in TTY (tele-type) mode).
;
; Service name 0x0e is stored in high register AH. The character
; that should be printed is stored in low register AL.
;
; Interrupt is called by using the CPU instruction "int".
;
mov ah, 0x0e            ; Set writing mode as TTY (tele-type) mode
mov al, 'H'             ; Write character to the screen
int 0x10
mov al, 'E'             ; Write character to the screen
int 0x10
mov al, 'L'             ; Write character to the screen
int 0x10
mov al, 'L'             ; Write character to the screen
int 0x10
mov al, 'O'             ; Write character to the screen
int 0x10

jmp $                   ; Jump to the current position within program
times 510-($-$$) db 0   ; Pad out our program from current position until
                        ; the 510th byte with enough zeros bytes (db 0).
                        ; "$" marks the current position within program.
                        ; "$$" marks the beginning of the current section.
dw 0xaa55               ; The last two bytes are magic number 0xaa and 0x55.
